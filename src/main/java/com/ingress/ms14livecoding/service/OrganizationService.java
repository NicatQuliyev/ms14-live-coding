package com.ingress.ms14livecoding.service;

import com.ingress.ms14livecoding.dto.OrganizationRequest;
import com.ingress.ms14livecoding.dto.OrganizationResponse;
import com.ingress.ms14livecoding.dto.OrganizationSpecification;
import com.ingress.ms14livecoding.dto.SearchCriteria;
import com.ingress.ms14livecoding.entity.Organization;
import com.ingress.ms14livecoding.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrganizationService {

    private final OrganizationRepository organizationRepository;

    private final ModelMapper modelMapper;

    public List<OrganizationResponse> findAll() {
        return organizationRepository
                .findAll()
                .stream()
                .map(organization -> modelMapper.map(organization, OrganizationResponse.class))
                .collect(Collectors.toList());
    }

    public List<Organization> getAllByCriteria(List<SearchCriteria> dto) {
        OrganizationSpecification organizationSpecification = new OrganizationSpecification();

        dto.forEach(organizationSpecification::add);

        return organizationRepository.findAll(organizationSpecification);
    }

    public OrganizationResponse findById(Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException(
                String.format("Organization not found by id -%s", organizationId)
        ));

        OrganizationResponse organizationResponse = modelMapper.map(organization, OrganizationResponse.class);

        return organizationResponse;
    }

    public OrganizationResponse save(OrganizationRequest request) {
        Organization organization = modelMapper.map(request, Organization.class);

        return modelMapper.map(organizationRepository.save(organization), OrganizationResponse.class);
    }

    public OrganizationResponse update(Long organizationId, OrganizationRequest request) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException(
                String.format("Organization not found by id -%s", organizationId)
        ));

        modelMapper.map(request, organization, "map");
        organization.setId(organizationId);

        return modelMapper.map(organizationRepository.save(organization), OrganizationResponse.class);
    }

    public void delete(Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException(
                String.format("Organization not found by id -%s", organizationId)
        ));

        organizationRepository.delete(organization);
    }


}
