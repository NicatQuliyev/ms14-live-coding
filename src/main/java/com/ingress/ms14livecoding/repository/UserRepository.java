package com.ingress.ms14livecoding.repository;

import com.ingress.ms14livecoding.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> , JpaSpecificationExecutor<User> {

//    @Query(value = "select u from User u join fetch u.organization o where o.id = :id")
//    User findAllByOrganizationId(Long id);


    Optional<User> findByOrganizationIdAndId(Long organizationId, Long userId);

}
