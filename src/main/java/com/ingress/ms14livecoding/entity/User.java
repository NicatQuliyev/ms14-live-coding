package com.ingress.ms14livecoding.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    String surname;

    String email;

    String password;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    Organization organization;

//    @ManyToMany(mappedBy = "users")
//    @JsonIgnore
//    @ToString.Exclude
//    Set<Task> task;


}
