package com.ingress.ms14livecoding.repository;

import com.ingress.ms14livecoding.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OrganizationRepository extends JpaRepository<Organization, Long> , JpaSpecificationExecutor<Organization> {
}
