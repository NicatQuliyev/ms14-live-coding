package com.ingress.ms14livecoding.controller;

import com.ingress.ms14livecoding.dto.SearchCriteria;
import com.ingress.ms14livecoding.dto.UserRequest;
import com.ingress.ms14livecoding.dto.UserResponse;
import com.ingress.ms14livecoding.entity.Organization;
import com.ingress.ms14livecoding.entity.User;
import com.ingress.ms14livecoding.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<UserResponse>> findAll() {
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<User> getListByCriteria(@RequestBody List<SearchCriteria> dto) {
        return userService.getAllByCriteria(dto);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserResponse> findById(@PathVariable Long userId) {
        return new ResponseEntity<>(userService.findById(userId), HttpStatus.OK);
    }

    @PostMapping("/organization/{organizationId}")
    public ResponseEntity<UserResponse> save(@RequestBody @Validated UserRequest request,
                                             @PathVariable Long organizationId) {
        return new ResponseEntity<>(userService.save(request,organizationId), HttpStatus.CREATED);
    }

    @PutMapping("/{userId}")
    public ResponseEntity<UserResponse> update(@RequestBody UserRequest request, @PathVariable Long userId) {
        return new ResponseEntity<>(userService.update(request, userId), HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public void delete(@PathVariable Long userId) {
        userService.delete(userId);
    }
}
