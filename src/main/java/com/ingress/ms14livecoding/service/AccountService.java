package com.ingress.ms14livecoding.service;

import com.ingress.ms14livecoding.entity.Account;
import com.ingress.ms14livecoding.entity.PhoneNumber;
import com.ingress.ms14livecoding.repository.AccountRepository;
import com.ingress.ms14livecoding.repository.PhoneNumberRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;

    private final PhoneNumberRepository phoneNumberRepository;

    public void assingPhonetoUsers(Long accountId, List<Long> phoneNumberIds) {
        Account account = accountRepository.findById(accountId).orElseThrow();

        phoneNumberIds.forEach(i -> {
            PhoneNumber referenceById = phoneNumberRepository.getReferenceById(i);
//            referenceById.setAccount(account);
            account.getPhoneNumberList().add(referenceById);
        });

        accountRepository.save(account);

    }

    public void transferProxy(Long fromId, Long toId, Double amount) throws Exception {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        try {
            Account from = em.find(Account.class, fromId);
            Account to = em.find(Account.class, toId);
            transfer(from, to, amount);
        } catch (RuntimeException e) {
            em.getTransaction().rollback();
        } finally {
            em.getTransaction().commit();
            em.close();
        }
    }

    public void transfer(Account from, Account to, Double amount) throws Exception {
        if (from.getBalance() < amount) {
            throw new RuntimeException("balance not enough");
        }

        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + amount);
        log.info("waiting for 5 second");
//        asdasda
        Thread.sleep(5000);

    }


}
