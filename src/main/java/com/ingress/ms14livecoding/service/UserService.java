package com.ingress.ms14livecoding.service;

import com.ingress.ms14livecoding.dto.SearchCriteria;
import com.ingress.ms14livecoding.dto.UserRequest;
import com.ingress.ms14livecoding.dto.UserResponse;
import com.ingress.ms14livecoding.dto.UserSpecification;
import com.ingress.ms14livecoding.entity.Organization;
import com.ingress.ms14livecoding.entity.User;
import com.ingress.ms14livecoding.repository.OrganizationRepository;
import com.ingress.ms14livecoding.repository.TaskRepository;
import com.ingress.ms14livecoding.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final TaskRepository taskRepository;

    private final OrganizationRepository organizationRepository;

    private final ModelMapper modelMapper;


    public List<UserResponse> findAll() {
        return userRepository
                .findAll()
                .stream()
                .map(user -> modelMapper.map(user, UserResponse.class))
                .collect(Collectors.toList());
    }

    public UserResponse findById(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException(
                String.format("User nor found by id -%s", userId)
        ));

        UserResponse userResponse = modelMapper.map(user, UserResponse.class);

        return userResponse;
    }

    public UserResponse save(UserRequest userRequest, Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException(
                String.format("Organization nor found by id -%s", organizationId)
        ));

        User user = modelMapper.map(userRequest, User.class);
        user.setOrganization(organization);

        return modelMapper.map(userRepository.save(user),UserResponse.class);
    }

    public UserResponse update(UserRequest userRequest, Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException(
                String.format("User nor found by id -%s", userId)
        ));

        modelMapper.map(userRequest,user,"map");
        user.setId(userId);

        return modelMapper.map(userRepository.save(user),UserResponse.class);
    }

    public void delete(Long userId) {
        var tasks = taskRepository.findAllByUserId(userId);
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException(
                String.format("User nor found by id -%s", userId)
        ));

        tasks.forEach(task -> {
            task.getUsers().remove(user);
        });

        taskRepository.saveAll(tasks);


        userRepository.delete(user);
    }
    public List<User> getAllByCriteria(List<SearchCriteria> dto) {
        UserSpecification userSpecification = new UserSpecification();

        dto.forEach(userSpecification::add);

        return userRepository.findAll(userSpecification);
    }
}
