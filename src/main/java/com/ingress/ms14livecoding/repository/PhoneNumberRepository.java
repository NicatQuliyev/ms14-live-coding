package com.ingress.ms14livecoding.repository;

import com.ingress.ms14livecoding.entity.Account;
import com.ingress.ms14livecoding.entity.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {


}
