package com.ingress.ms14livecoding.controller;

import com.ingress.ms14livecoding.entity.Account;
import com.ingress.ms14livecoding.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountRepository accountRepository;

    @GetMapping("/{id}")
    public Account getAccount(@PathVariable Long id) {
        return accountRepository.findById(id).get();
    }

    @GetMapping
    public List<Account> getAllAccount() {
        return accountRepository.findAll();
    }
}
