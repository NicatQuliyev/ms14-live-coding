package com.ingress.ms14livecoding.dto;

import com.ingress.ms14livecoding.entity.TaskStatus;
import com.ingress.ms14livecoding.entity.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class TaskResponse {

    Long id;

    String name;

    String deadline;

    TaskStatus taskStatus;

    String description;

   // Organization organization;

    Set<User> users = new HashSet<>();
}
