package com.ingress.ms14livecoding.controller;

import com.ingress.ms14livecoding.dto.*;
import com.ingress.ms14livecoding.entity.Organization;
import com.ingress.ms14livecoding.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/organizations")
@RequiredArgsConstructor
public class OrganizationController {

    private final OrganizationService organizationService;

    @GetMapping
    public ResponseEntity<List<OrganizationResponse>> findAll() {
        return new ResponseEntity<>(organizationService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Organization> getListByCriteria(@RequestBody List<SearchCriteria> dto) {
        return organizationService.getAllByCriteria(dto);
    }

    @GetMapping("/{organizationId}")
    public ResponseEntity<OrganizationResponse> findById(@PathVariable Long organizationId) {
        return new ResponseEntity<>(organizationService.findById(organizationId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OrganizationResponse> save(@RequestBody OrganizationRequest request) {
        return new ResponseEntity<>(organizationService.save(request), HttpStatus.CREATED);
    }

    @PutMapping("/{organizationId}")
    public ResponseEntity<OrganizationResponse> update(@RequestBody OrganizationRequest request, @PathVariable Long organizationId) {
        return new ResponseEntity<>(organizationService.update(organizationId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{organizationId}")
    public void delete(@PathVariable Long organizationId) {
        organizationService.delete(organizationId);
    }
}
