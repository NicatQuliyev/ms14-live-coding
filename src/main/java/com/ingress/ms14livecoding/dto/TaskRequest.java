package com.ingress.ms14livecoding.dto;

import com.ingress.ms14livecoding.entity.TaskStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class TaskRequest {
    String name;

    String deadline;

    TaskStatus taskStatus;

    String description;
}
