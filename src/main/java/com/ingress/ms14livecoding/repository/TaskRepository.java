package com.ingress.ms14livecoding.repository;

import com.ingress.ms14livecoding.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {

    @Query(value = "select t from Task t join fetch t.users u where u.id = :id")
    Collection<Task> findAllByUserId(Long id);

    Optional<Task> findByOrganizationIdAndId(Long organizationId, Long taskId);

}
