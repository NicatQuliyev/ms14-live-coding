package com.ingress.ms14livecoding.service;

import com.ingress.ms14livecoding.dto.*;
import com.ingress.ms14livecoding.entity.Organization;
import com.ingress.ms14livecoding.entity.Task;
import com.ingress.ms14livecoding.entity.User;
import com.ingress.ms14livecoding.repository.OrganizationRepository;
import com.ingress.ms14livecoding.repository.TaskRepository;
import com.ingress.ms14livecoding.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    private final OrganizationRepository organizationRepository;

    private final UserRepository userRepository;

    private final ModelMapper modelMapper;

    public List<TaskResponse> findAll() {
        return taskRepository
                .findAll()
                .stream()
                .map(task -> modelMapper.map(task, TaskResponse.class))
                .collect(Collectors.toList());
    }

    public List<Task> getAllByCriteria(List<SearchCriteria> dto) {
        TaskSpecification taskSpecification = new TaskSpecification();

        dto.forEach(taskSpecification::add);

        return taskRepository.findAll(taskSpecification);
    }

    public TaskResponse findById(Long id) {
        Task task = taskRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Task not found with id %s", id)
        ));

        TaskResponse taskResponse = modelMapper.map(task, TaskResponse.class);

        return taskResponse;
    }

    public TaskResponse save(TaskRequest request, Long organizationId) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(() -> new RuntimeException(
                String.format("Organization nor found by id -%s", organizationId)
        ));

        Task task = modelMapper.map(request, Task.class);
        task.setOrganization(organization);

        return modelMapper.map(taskRepository.save(task), TaskResponse.class);
    }

    public TaskResponse update(Long id, TaskRequest request) {
        Task task = taskRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Task not found with id %s", id)
        ));

        modelMapper.map(request, task, "map");
        task.setId(id);

        return modelMapper.map(taskRepository.save(task), TaskResponse.class);
    }

    public void delete(Long id) {
        Task task = taskRepository.findById(id).orElseThrow(() -> new RuntimeException(
                String.format("Task not found with id %s", id)
        ));

        taskRepository.delete(task);
    }

    @Transactional
    public Task assignUsersToTask(Long organizationId, Long taskId, UserListDto userListDto) {
        Task task = taskRepository.findByOrganizationIdAndId(organizationId, taskId)
                .orElseThrow(() -> new RuntimeException("Task not found"));

        for (Long userId : userListDto.getUserIds()) {
            User user = userRepository.findByOrganizationIdAndId(organizationId, userId)
                    .orElseThrow(() -> new RuntimeException("User not found"));
            task.getUsers().add(user);
        }

        return taskRepository.save(task);
    }
}
