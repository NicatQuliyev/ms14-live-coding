package com.ingress.ms14livecoding;

import com.ingress.ms14livecoding.dto.UserListDto;
import com.ingress.ms14livecoding.entity.Account;
import com.ingress.ms14livecoding.entity.PhoneNumber;
import com.ingress.ms14livecoding.repository.AccountRepository;
import com.ingress.ms14livecoding.service.AccountService;
import com.ingress.ms14livecoding.service.TaskService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class Ms14LiveCodingApplication implements CommandLineRunner {

    private final AccountService accountService;
    private final AccountRepository accountRepository;

    private final EntityManagerFactory emf;

    private final TaskService taskService;


    public static void main(String[] args) {
        SpringApplication.run(Ms14LiveCodingApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

//        Account account = new Account();
//        account.setName("Tofig");
//        account.setBalance(0.0);
//
//        Account save = accountRepository.save(account);

//        Account referenceById = accountRepository.getReferenceById(1L);
//        referenceById.setName("Fariz");
//
//        System.out.println(save.getPhoneNumberList());
//
//        log.info("count");
//        accountService.assingPhonetoUsers(save.getId(), List.of(1L,2L,3L,4L));

        //bunu sual ver
//        log.info("#1 getting account with id 1");
//        var account1 = accountRepository.findByName("Nicat");
//        log.info("account is {}", account1);
//
//        log.info("#2 getting account with id 1");
//        var account2 = accountRepository.findById(1L);
//        log.info("account is {}", account2);

//        ----------------------------------------------------
//        log.info("#1 getting account with id 1");
//        var account1 = accountRepository.findById(1L);
//        log.info("account is {}", account1);
//
//        log.info("#2 getting account with id 1");
//        var account2 = accountRepository.findAll();
//        log.info("account is {}", account2);


//        EntityManager em = emf.createEntityManager();
//        em.getTransaction().begin();
//        Account account = em.find(Account.class, 1L);
//        account.setName("Nicat");
//        em.flush();
//        em.detach(account);
////        em.merge(account);
//        account.setName("Elgun");
//        em.getTransaction().commit();
//        em.close();


    }


}
