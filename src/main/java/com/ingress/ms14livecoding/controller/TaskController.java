package com.ingress.ms14livecoding.controller;

import com.ingress.ms14livecoding.dto.SearchCriteria;
import com.ingress.ms14livecoding.dto.TaskRequest;
import com.ingress.ms14livecoding.dto.TaskResponse;
import com.ingress.ms14livecoding.dto.UserListDto;
import com.ingress.ms14livecoding.entity.Task;
import com.ingress.ms14livecoding.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/tasks")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @GetMapping
    public ResponseEntity<List<TaskResponse>> findAll() {
        return new ResponseEntity<>(taskService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Task> getListByCriteria(@RequestBody List<SearchCriteria> dto) {
        return taskService.getAllByCriteria(dto);
    }

    @GetMapping("/{taskId}")
    public ResponseEntity<TaskResponse> findById(@PathVariable Long taskId) {
        return new ResponseEntity<>(taskService.findById(taskId), HttpStatus.OK);
    }

    @PostMapping("/organization/{organizationId}")
    public ResponseEntity<TaskResponse> save(@PathVariable Long organizationId,
                                             @RequestBody TaskRequest request) {
        return new ResponseEntity<>(taskService.save(request, organizationId), HttpStatus.CREATED);
    }

    @PostMapping("/organizations/{organizationId}/tasks/{taskId}/users")
    public ResponseEntity<Task> assignUsersToTask(@PathVariable Long organizationId,
                                                  @PathVariable Long taskId,
                                                  @RequestBody UserListDto userIds) {

        return new ResponseEntity<>(taskService.assignUsersToTask(organizationId, taskId, userIds), HttpStatus.CREATED);
    }

    @PutMapping("/{taskId}")
    public ResponseEntity<TaskResponse> update(@PathVariable Long taskId,
                                               @RequestBody TaskRequest request) {
        return new ResponseEntity<>(taskService.update(taskId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{taskId}")
    public void delete(@PathVariable Long taskId) {
        taskService.delete(taskId);
    }

}
