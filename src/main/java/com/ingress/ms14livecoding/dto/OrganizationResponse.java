package com.ingress.ms14livecoding.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationResponse {
    Long id;

    String name;

   // Set<TaskResponse> tasks = new HashSet<>();

    Set<UserResponse> users = new HashSet<>();
}
